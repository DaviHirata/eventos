<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Eventos em Santa Maria</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

  <div class="container mt-5">
    <h1>Eventos em Santa Maria</h1>

    <?php
      // Verificar se o cookie de aceitação está definido
      $cookieAceito = isset($_COOKIE['aceitou_cookies']) && $_COOKIE['aceitou_cookies'] == 'sim';

      if (!$cookieAceito) {
        echo '<div id="cookie-banner" class="alert alert-info" role="alert">';
        echo 'Este site utiliza cookies. <button class="btn btn-sm btn-primary ml-2" onclick="aceitarCookies()">Aceitar</button> <button class="btn btn-sm btn-danger ml-2" onclick="recusarCookies()">Recusar</button>';
        echo '</div>';
      }
    ?>

    <div class="card-deck mt-4">
      <?php
        $eventos = [
          [
            'nome' => 'MAIARA E MARAISA EM SANTA MARIA/RS',
            'localizacao' => 'Espaço Vivaz',
            'data' => '24-11-2023',
            'organizador' => 'Minha Entrada',
            'ingresso' => '<a href="https://minhaentrada.com.br/evento/maiara-e-maraisa-em-santa-mariars-22123">Mais Informações</a>',
          ],
          [
            'nome' => 'PRÉ PARADA ALTERNATIVA LGBT+',
            'localizacao' => 'Rockers Soul Food',
            'data' => '01-12-2023',
            'organizador' => 'Coletivo Voe feat. Farofei',
            'ingresso' => '<a href="https://www.sympla.com.br/evento/pre-parada-alternativa-lgbt-coletivo-voe-feat-farofei/2244500?referrer=www.google.com">Mais Informações</a>',
          ]
        ];

        foreach ($eventos as $evento) {
          echo '<div class="card">';
          echo '<div class="card-body">';
          echo '<h5 class="card-title">' . $evento['nome'] . '</h5>';
          echo '<p class="card-text">Local: ' . $evento['localizacao'] . '</p>';
          echo '<p class="card-text">Data: ' . $evento['data'] . '</p>';
          echo '<p class="card-text">Organizador: ' . $evento['organizador'] . '</p>';
          echo '<p class="card-text">Ingressos: ' . $evento['ingresso'] . '</p>';

          // Verificar se o usuário está interessado no evento
          $cookieEvento = 'interesse_' . str_replace(' ', '_', strtolower($evento['nome']));
          $interessado = isset($_COOKIE[$cookieEvento]) && $_COOKIE[$cookieEvento] == 'sim';

          // Botão "Estou Interessado"
          $botaoTexto = $interessado ? 'Interessado' : 'Estou Interessado';
          $botaoClasse = $interessado ? 'btn-secondary' : 'btn-primary';

          echo '<button class="btn ' . $botaoClasse . '" onclick="marcarInteresse(\'' . $evento['nome'] . '\')" ' . ($cookieAceito ? '' : 'disabled') . '>' . $botaoTexto . '</button>';

          echo '</div>';
          echo '</div>';
        }
      ?>
    </div>
  </div>

  <script>
    function aceitarCookies() {
      // Adicione lógica para armazenar aceitação de cookies aqui
      // Neste exemplo, apenas exibiremos um alerta
      alert('Cookies aceitos!');

      // Definir cookie de aceitação com validade de 30 dias
      var dataExpiracao = new Date();
      dataExpiracao.setDate(dataExpiracao.getDate() + 30);
      document.cookie = 'aceitou_cookies=sim; expires=' + dataExpiracao.toUTCString() + '; path=/';

      // Ocultar o banner de cookies
      document.getElementById('cookie-banner').style.display = 'none';

      // Recarregar a página após aceitar cookies
      location.reload();
    }

    function recusarCookies() {
      // Adicione lógica para lidar com a recusa de cookies aqui
      // Neste exemplo, apenas exibiremos um alerta
      alert('Cookies recusados!');

      // Recarregar a página após recusar cookies
      location.reload();
    }

    function marcarInteresse(eventoNome) {
      // Verificar se os cookies foram aceitos antes de marcar interesse
      if (document.cookie.indexOf('aceitou_cookies=sim') === -1) {
        alert('Você precisa aceitar os cookies para marcar interesse.');
        return;
      }

      // Adicione lógica para armazenar interesse em cookies aqui
      // Neste exemplo, apenas exibiremos um alerta
      alert('Você está interessado em: ' + eventoNome);

      // Definir cookie de interesse para este evento
      var dataExpiracao = new Date();
      dataExpiracao.setDate(dataExpiracao.getDate() + 30);
      document.cookie = 'interesse_' + eventoNome.replace(/ /g, '_').toLowerCase() + '=sim; expires=' + dataExpiracao.toUTCString() + '; path=/';

      // Recarregar a página após marcar interesse
      location.reload();
    }
  </script>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>