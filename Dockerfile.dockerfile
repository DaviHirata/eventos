# Use the official PHP image
FROM php:7.4-apache

# Set the working directory to /var/www/html
WORKDIR /var/www/html

# Copy the local code to the container
COPY ./eventos /var/www/html

# Enable Apache modules
RUN a2enmod rewrite

# Install any additional dependencies here (if needed)

# Expose port 80
EXPOSE 80

# Start Apache in the foreground
CMD ["apache2-foreground"]